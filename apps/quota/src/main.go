package main

import (
	"quota/src/module"
)

// @title Gest Example API
// @version 1.0
// @description This is a sample swagger for Fiber
// @termsOfService http://swagger.io/terms/
// @contact.name API Support
// @contact.email fiber@swagger.io
// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html
// @securityDefinitions.basic BasicAuth
// @in header
// @name Authorization
func main() {
	app := module.NewApp()
	app.Run()
}
